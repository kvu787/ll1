package grammar

import (
	"unicode/utf8"

	. "bitbucket.org/kvu787/ll1/utility"
)

type Rule []string

func (r Rule) Lhs() string {
	return r[0]
}

func (r Rule) Rhs() []string {
	return r[1:]
}

type Grammar []Rule

func (g Grammar) IsTerminal(symbol string) bool {
	result := true
	for _, rule := range g {
		if rule.Lhs() == symbol {
			result = false
		}
	}
	return result
}

/*
	Types of sets
	- empty: empty set
	- epsilon: set with only epsilon
	- character: set with 1 or more characters
	- epsilon U character
*/

/*
	valid
	- epsilon
	- character
	- epsilon U character
*/
func (g Grammar) PredictSet(rule Rule) map[rune]int {
	firstSet := g.RhsFirstSet(rule.Rhs())
	if _, found := firstSet[0]; found {
		return Union(firstSet, g.FollowSet(rule.Lhs()))
	} else {
		return firstSet
	}
}

/*
	valid
	- empty
	- epsilon
	- character
	- epsilon U character
*/
func (g Grammar) FollowSet(lhs string) map[rune]int {
	type Tuple struct {
		lhs         string // nonterminal that contains the pair
		leftSymbol  string
		rightSymbol string
	}

	GetTupleFollowSet := func(tuple Tuple, lhsFind string, g Grammar) map[rune]int {
		if tuple.leftSymbol == lhsFind {
			if tuple.rightSymbol == "" {
				return g.FollowSet(tuple.lhs)
			} else if g.IsTerminal(tuple.rightSymbol) {
				r, _ := utf8.DecodeRuneInString(tuple.rightSymbol)
				return map[rune]int{r: 0}
			} else { // nonterminal
				return g.LhsFirstSet(tuple.rightSymbol)
			}
		} else {
			return map[rune]int{}
		}
	}

	// enumerate all (lhs, left, right) tuples
	tuples := []Tuple{}
	for _, rule := range g {
		var pairs [][2]string = GetPairs(rule.Rhs())
		for _, pair := range pairs {
			if pair[1] == "" && pair[0] == rule.Lhs() {
				continue
			}
			tuples = append(tuples, Tuple{rule.Lhs(), pair[0], pair[1]})
		}
	}

	// merge all tuple follow sets
	result := map[rune]int{}
	for _, tuple := range tuples {
		tupleFollowSet := GetTupleFollowSet(tuple, lhs, g)
		result = Union(result, tupleFollowSet)
	}
	return result
}

/*
	valid
	- epsilon
	- character
	- epsilon U character
*/
func (g Grammar) LhsFirstSet(lhs string) map[rune]int {
	result := map[rune]int{}
	for _, rule := range g {
		if rule.Lhs() == lhs {
			firstSet := g.RhsFirstSet(rule.Rhs())
			result = Union(result, firstSet)
		}
	}
	if len(result) == 0 {
		panic("cannot return empty set")
	}
	return result
}

/*
	valid
	- epsilon
	- character
*/
func (g Grammar) RhsFirstSet(rhs []string) map[rune]int {
	if len(rhs) == 0 {
		return map[rune]int{0: 0}
	} else {
		front := rhs[0]
		rest := rhs[1:]
		if g.IsTerminal(front) {
			r, _ := utf8.DecodeRuneInString(front)
			return map[rune]int{r: 0}
		} else {
			firstSet := g.LhsFirstSet(front)
			if _, found := firstSet[0]; found {
				return g.RhsFirstSet(rest)
			} else {
				return firstSet
			}
		}
	}
}
