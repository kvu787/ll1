/*

d: lisp program -> f: compiler -> d: machine code -> f: processor -> d: output
d: lisp program -> f: interpreter -> d: output
d: c program -> f: compiler -> d: machine code, python interpreter
d: python program -> f: asm python interpreter -> d: output

*/

package ll1

import (
	"container/list"
	"unicode/utf8"

	. "bitbucket.org/kvu787/ll1/grammar"
	. "bitbucket.org/kvu787/ll1/utility"
)

var SampleGrammar Grammar = []Rule{
	{"s", "e", "endm"},
	{"e", "t", "e*"},
	{"e*", "+", "t", "e*"},
	{"e*", "-", "t", "e*"},
	{"e*"},
	{"t", "f", "t*"},
	{"t*", "*", "f", "t*"},
	{"t*", "/", "f", "t*"},
	{"t*"},
	{"f", "(", "e", ")"},
	{"f", "1"},
}

type parseTree struct {
	Data     string
	Children *list.List // parseTree, list with len == 0 indicates no children
}

func (pt parseTree) toString(currentPadding, padding int) string {
	result := StringRepeat(currentPadding, " ") + pt.Data
	if pt.Children.Len() == 0 {
		return result
	}
	for child := pt.Children.Front(); child != nil; child = child.Next() {
		result += "\n" + child.Value.(parseTree).toString(currentPadding+padding, padding)
	}
	return result
}

func (pt parseTree) String() string {
	return pt.toString(0, 3)
}

type ProcessedGrammar struct {
	Grammar                        // rule_id -> rule
	nonterminalToId map[string]int // nonterminal -> nonterminal_id
	lookaheadToId   map[rune]int   // lookahead -> lookahead_id
	table           [][]int        // nonterminal_id x lookahead_id -> rule_id
}

func (pg ProcessedGrammar) ChooseRule(nonterminal string, lookahead rune) Rule {
	nonterminalId := pg.nonterminalToId[nonterminal]
	lookaheadId := pg.lookaheadToId[lookahead]
	ruleId := pg.table[nonterminalId][lookaheadId]
	return pg.Grammar[ruleId]
}

func ProcessGrammar(g Grammar) ProcessedGrammar {
	// assign unique, contiguous id's to each nonterminal
	nonterminalToId := map[string]int{}
	{
		i := 0
		for _, rule := range g {
			if _, found := nonterminalToId[rule.Lhs()]; !found {
				nonterminalToId[rule.Lhs()] = i
				i++
			}
		}
	}

	// assign unique, contiguous id's to each possible lookahead
	lookaheadToId := map[rune]int{}
	{
		i := 0
		for _, rule := range g {
			for _, s := range rule.Rhs() {
				if g.IsTerminal(s) {
					r, _ := utf8.DecodeRuneInString(s)
					if _, found := lookaheadToId[r]; !found {
						lookaheadToId[r] = i
						i++
					}
				}
			}
		}
		// set null character
		lookaheadToId['\000'] = i
	}

	table := make([][]int, len(nonterminalToId))
	for i := 0; i < len(table); i++ {
		table[i] = make([]int, len(lookaheadToId))
	}

	// fill table with -1
	for r := 0; r < len(nonterminalToId); r++ {
		for c := 0; c < len(lookaheadToId); c++ {
			table[r][c] = -1
		}
	}

	// for each rule, fill data for nonterminal in table
	for ruleId, rule := range g {
		nonterminalId := nonterminalToId[rule.Lhs()]
		predictSet := g.PredictSet(rule)
		for lookahead, _ := range predictSet {
			lookaheadId, found := lookaheadToId[lookahead]
			if !found {
				panic("no id found for lookahead")
			}
			if table[nonterminalId][lookaheadId] != -1 {
				panic("attempted to set rule twice")
			}
			table[nonterminalId][lookaheadId] = ruleId
		}
	}

	return ProcessedGrammar{
		g,
		nonterminalToId,
		lookaheadToId,
		table,
	}
}

func Parse(pg ProcessedGrammar, input string) (parseTree, string) {
	type Tuple struct {
		lhs string
		dhs *list.List // parseTree
		rhs []string
	}

	/*
		invariants
		let Tuple be the 3-tuple {lhs string, dhs list, rhs list}
		let stack be the list containing the single element: {startRule.lhs, {}, startRule.rhs}
		let input represent the source string
		then parse tree of startRule is equivalent to these operations:
			mapping the operation: parse rhs into dhs => create tree {lhs, children}
			accumulate, from low to high indices, bot.dhs.append(top)
	*/
	startRule := pg.Grammar[0]
	var stack *list.List = list.New()
	stack.PushFront(Tuple{startRule.Lhs(), list.New(), startRule.Rhs()})
	for {
		var topTuple Tuple = stack.Remove(stack.Front()).(Tuple)
		if len(topTuple.rhs) == 0 {
			// this rule is parsed
			if stack.Len() == 0 {
				// the base rule is parsed
				return parseTree{topTuple.lhs, topTuple.dhs}, input
			} else {
				// add rule to parent parse list
				stack.
					Front().
					Value.(Tuple).
					dhs.
					PushBack(parseTree{topTuple.lhs, topTuple.dhs})
			}
		} else {
			// this rule is not fully parsed
			var rhsFrontSym string = topTuple.rhs[0]
			topTuple.rhs = topTuple.rhs[1:]
			if pg.IsTerminal(rhsFrontSym) {
				// push a leaf tree to dhs, consume input
				topTuple.dhs.PushBack(parseTree{rhsFrontSym, list.New()})
				if len(input) == 0 {
					// do nothing if input is empty
				} else {
					_, firstRuneSize := utf8.DecodeRuneInString(input)
					input = input[firstRuneSize:]
				}
				// push top back onto stack
				stack.PushFront(topTuple)
			} else {
				// choose a rule based on lookahead and push the rule to the stack
				var lookahead rune
				if len(input) == 0 {
					lookahead = '\000'
				} else {
					lookahead, _ = utf8.DecodeRuneInString(input)
				}
				var rhsFrontRule Rule = pg.ChooseRule(rhsFrontSym, lookahead)
				// push top
				stack.PushFront(topTuple)
				// push new stack
				stack.PushFront(Tuple{rhsFrontRule.Lhs(), list.New(), rhsFrontRule.Rhs()})
			}
		}
	}
}
