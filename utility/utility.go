package utility

func GetPairs(stringList []string) [][2]string {
	if len(stringList) == 0 {
		return [][2]string{}
	}
	toProcess := stringList
	result := [][2]string{}
	for {
		switch len(toProcess) {
		case 0:
			return result
		case 1:
			result = append(result, [2]string{toProcess[0], ""})
			toProcess = toProcess[1:]
		default:
			result = append(result, [2]string{toProcess[0], toProcess[1]})
			toProcess = toProcess[1:]
		}
	}
}

func Union(s1, s2 map[rune]int) map[rune]int {
	result := make(map[rune]int, len(s1)+len(s2))
	for k, _ := range s1 {
		result[k] = 0
	}
	for k, _ := range s2 {
		result[k] = 0
	}
	return result
}

func StringRepeat(n int, s string) string {
	if n == 0 {
		return ""
	} else if n%2 == 0 {
		return StringRepeat(n/2, s) + StringRepeat(n/2, s)
	} else {
		return s + StringRepeat(n-1, s)
	}
}
